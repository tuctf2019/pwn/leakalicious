# leakalicious

Desc: `King Arthur has proposed this challenge for all those who consider themselves worthy.`

Architecture: x86

Given files:

* leakalicious

Hints:

* BLUKAT ME! If only we knew what libc to use...

Flag: `TUCTF{cl0udy_w17h_4_ch4nc3_0f_l1bc}`
