#include <stdio.h>  // prints
#include <stdlib.h> // malloc
#include <string.h> // strcmp
#include <unistd.h> // read
#include <fcntl.h>  // open
#include <unistd.h> // close
#include <time.h>   // time

int main() {
    setvbuf(stdout, NULL, _IONBF, 20);
    setvbuf(stdin, NULL, _IONBF, 20);

	puts("Stop. Who would pwn this binary must answer me these questions three, ere /dev/null he see.\n");

	char buf[32];
	// This places the address of puts@libc() on the stack
	// this var can't be named puts;
	void *pts = &puts;

	printf("What... is your handle?\n> ");
	read(0, buf, 64);
	printf("hmmm... %s?\n", buf);

	printf("What... is your exploit?\n> ");
	read(0, buf, 64);
	
	printf("What... version of libc am I using?\n> ");
	read(0, buf, 64);


	return 0;
}
